import React from "react";
import { useState } from "react";

import "./App.css";

const USER_FORM_DEFAULT = {
  pseudo:"",
  age:"",
  message:"",
};

function App() {

  const [userForm, setUserForm ] = useState(USER_FORM_DEFAULT);
  const [users, setUsers] = useState([]);

  function handleUserPseudoForm(event) {
    setUserForm({
      ...userForm,
      pseudo: event.target.value
    })
  }

  function handleUserAgeForm(event) {
    setUserForm({
      ...userForm,
      age: event.target.value
    })
  }

  function handleUserMessageForm(event) {
    setUserForm({
      ...userForm,
      message: event.target.value
    })
  }

  function userCreatedOnSubmit(e) {
    e.preventDefault()
    setUsers([...users, userForm])
    setUserForm(USER_FORM_DEFAULT);
  }

  return (
    <form className="form">
      <h1>My simple form</h1>
      <div>
        <input 
          className="pseudoForm"
          type="text" 
          placeholder="Enter your pseudo" 
          value={userForm.pseudo} 
          onChange={handleUserPseudoForm}/>
      </div>
      <div>
        <input 
          className="ageForm"
          type="number" 
          placeholder="Enter your age" 
          value={userForm.age} 
          onChange={handleUserAgeForm}/>
      </div> 
      <textarea 
        className="messageForm"
        placeholder="Enter your message" 
        value={userForm.message} 
        onChange={handleUserMessageForm}/>
      <div>
        <button onClick={userCreatedOnSubmit}>Envoyer</button>
      </div>
    </form>
  );
}

export default App;
